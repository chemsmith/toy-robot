<?php

namespace ToyRobot;

/**
 * Class Robot
 * @package ToyRobot
 */
class Robot implements ToyInterface
{
    /**
     * @var array|null
     */
    private ?array $location;

    /**
     * A array index pointer corresponding to the current orientation from self::$playingBoardMovements
     *
     * @var int|null
     */
    private ?int $orientation;

    /**
     * @var array
     */
    private array $playingBoardMovements;

    /**
     * @var int
     */
    private int $playingBoardSize;

    /**
     * Robot constructor.
     */
    public function __construct()
    {
        $this->location = null;
        $this->orientation = null;
    }

    /**
     * Sets the size of the board the robot is to be placed on.
     *
     * MUST be set before the robot is placed
     *
     * @param int $playingBoardSize
     */
    public function setPlayingBoardSize(int $playingBoardSize)
    {
        $this->playingBoardSize = $playingBoardSize;
    }

    /**
     * Sets the possible movements and orientations available to the robot.
     *
     * MUST be set before the robot is placed
     *
     * @param array $playingBoardMovements
     */
    public function setPlayingBoardMovements(array $playingBoardMovements)
    {
        $this->playingBoardMovements = $playingBoardMovements;
    }

    /**
     * Sets the robots location on the board, returning true if desired position is on the board and false if it's not
     *
     * @param array $robotLocation
     * @return bool
     */
    public function setLocation(array $robotLocation): bool
    {
        foreach ($robotLocation as $position) {
            if ($position > $this->playingBoardSize - 1)
                return false;
        }
        $this->location = $robotLocation;
        return true;
    }

    /**
     * Sets the orientation pointer to the array index corresponding to the string orientation
     *
     * @param string $orientation
     */
    public function setOrientation(string $orientation)
    {
        $this->orientation = array_search($orientation, array_keys($this->playingBoardMovements));
    }

    /**
     * @return array|null
     */
    public function getLocation(): ?array
    {
        return $this->location;
    }

    /**
     * @return string|null
     */
    public function getOrientation(): ?string
    {
        if (null !== $this->orientation)
            return array_keys($this->playingBoardMovements)[$this->orientation];
        return null;
    }

    /**
     * According to the available movements on the current board and the robots orientation, move location if it
     * doesn't move us past the edge of the board
     *
     * @param int $scale
     */
    public function move(int $scale = 1)
    {
        $newLocation = $this->location;
        foreach ($this->playingBoardMovements[$this->getOrientation()] as $coordinate => $movement) {
            $newLocation[$coordinate] = $newLocation[$coordinate] + $movement * $scale;
            if ($newLocation[$coordinate] >= $this->playingBoardSize or $newLocation[$coordinate] < 0)
                return;
        }
        $this->location = $newLocation;
    }

    public function turnLeft()
    {
        $this->rotate(-1);
    }

    public function turnRight()
    {
        $this->rotate(1);
    }

    /**
     * Rotates left or right, moving around the possible orientations provided by the board
     *
     * @param int $direction
     */
    private function rotate(int $direction)
    {
        if ($this->orientation === 0 and $direction === -1)
            $this->orientation = sizeof($this->playingBoardMovements) - 1;
        elseif ($this->orientation === (sizeof($this->playingBoardMovements) - 1) and $direction === 1)
            $this->orientation = 0;
        else
            $this->orientation += $direction;
    }
}
