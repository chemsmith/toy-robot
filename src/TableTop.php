<?php

namespace ToyRobot;


/**
 * Class TableTop
 * @package ToyRobot
 */
final class TableTop implements BoardInterface
{
    /**
     * An array mapping orientation with cartesian-plane movement
     * @var array
     */
    private static array $movementDirectionMatrix = [
        'NORTH' => ['x' => 0, 'y' => 1],
        'EAST' => ['x' => 1, 'y' => 0],
        'SOUTH' => ['x' => 0, 'y' => -1],
        'WEST' => ['x' => -1, 'y' => 0],
    ];

    /**
     * @var int
     */
    private static int $boardSize = 5;

    /**
     * @var array|string[]
     */
    private static array $validCommands = ['PLACE', 'RIGHT', 'LEFT', 'MOVE', 'REPORT'];

    /**
     * @var ToyInterface
     */
    private ToyInterface $toy;

    public function __construct(ToyInterface $toy)
    {
        $this->toy = $toy;
        $this->toy->setPlayingBoardMovements(self::$movementDirectionMatrix);
        $this->toy->setPlayingBoardSize(self::$boardSize);
    }

    /**
     * Checks the board to see if the toy is currently placed in a valid location
     *
     * @return bool
     */
    private function toyIsPlaced()
    {
        return $this->toy->getLocation() !== null;
    }

    /**
     * Places the toy on the board
     *
     * @param array $toy_location
     * @param string $toy_orientation
     */
    private function placeToy(array $toy_location, string $toy_orientation)
    {
        if ($this->toy->setLocation($toy_location))
            $this->toy->setOrientation($toy_orientation);
    }

    /**
     * If there is currently a toy placed on the board instruct it to turn left or right
     *
     * @param string $direction
     */
    private function turnToy(string $direction)
    {
        if ($this->toyIsPlaced()) {
            switch ($direction) {
                case 'LEFT':
                    $this->toy->turnLeft();
                    break;
                case 'RIGHT':
                    $this->toy->turnRight();
            }
        }
    }

    /**
     * If there is a toy on the board instruct it to move
     */
    private function moveToy()
    {
        if ($this->toyIsPlaced())
            $this->toy->move();
    }

    /**
     * If there is a toy on the board interrogate it for its current location and orientation
     */
    private function reportToyPosition()
    {
        if ($this->toyIsPlaced()) {
            $position = '';
            foreach ($this->toy->getLocation() as $coordinate) {
                $position .= $coordinate . ',';
            }
            $position .= $this->toy->getOrientation() . "\n";
            echo $position;
        }
    }

    /**
     * Handles valid actions and determines what the toy should do
     *
     * @param array $action
     */
    private function handleAction(array $action)
    {
        switch ($action[0]) {
            case 'LEFT':
            case 'RIGHT':
                $this->turnToy($action[0]);
                break;
            case 'PLACE':
                $coordinates = explode(',', $action[1]);
                $toyLocation = [];
                foreach (array_keys(reset(self::$movementDirectionMatrix)) as $pos => $key) {
                    $toyLocation[$key] = $coordinates[$pos];
                }
                $this->placeToy($toyLocation, $action[2]);
                break;
            case 'MOVE':
                $this->moveToy();
                break;
            case 'REPORT':
                $this->reportToyPosition();
                break;
        }
    }

    /**
     * Takes a line from STDIN or a file and checks if it's a valid command for this board, if so then determine what
     * the command is and handle the action
     *
     * @param string $actionLine
     */
    public function processAndHandleAction(string $actionLine)
    {
        $validCommandsStr = implode('|', self::$validCommands);
        $validDirectionsStr = implode('|', array_keys(self::$movementDirectionMatrix));
        preg_match("/^($validCommandsStr)(?:\s((?:\d,)+)($validDirectionsStr))?$/", strtoupper($actionLine), $action);
        if (!empty($action)) {
            $this->handleAction(array_slice($action, 1));
        }
    }
}
