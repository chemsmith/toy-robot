<?php

namespace ToyRobot;

/**
 * Class Simulation
 *
 * Constructs the requirements for running a simulation, namely a board and toy then passes the board commands from either
 * STDIN or a provided file.
 *
 * @package ToyRobot
 */
final class Simulation
{
    /**
     * The current type of board under simulation
     *
     * @var BoardInterface
     */
    private BoardInterface $board;

    /**
     * Simulation constructor.
     *
     * Can optionally be passed-in a board or toy type but if none is provided it will run a simulation using the TableTop
     * and Robot respectively.
     *
     * @param BoardInterface|null $boardType
     * @param ToyInterface|null $toyType
     */
    public function __construct(BoardInterface $boardType = null, ToyInterface $toyType = null)
    {
        if ($boardType === null)
            $boardType = TableTop::class;
        if ($toyType === null)
            $toyType = Robot::class;
        $this->board = new $boardType(new $toyType);
    }

    /**
     * @param array $argv
     */
    public function run(array $argv)
    {
        if (sizeof($argv) === 2 && file_exists($argv[1])) {
            $fhandle = fopen($argv[1], 'r');
        } else {
            $fhandle = fopen('php://stdin', 'r');
        }

        while (($commandLine = fgets($fhandle))) {
            $this->board->processAndHandleAction($commandLine);
        }

        fclose($fhandle);
    }
}


