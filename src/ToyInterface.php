<?php


namespace ToyRobot;


interface ToyInterface
{
    public function getLocation() : ?array;

    public function getOrientation() : ?string;

    public function setLocation(array $robotLocation) : bool;

    public function setOrientation(string $orientation);

    public function setPlayingBoardSize(int $playingBoardSize);

    public function setPlayingBoardMovements(array $playingBoardMovements);

    public function move(int $scale = 1);

    public function turnLeft();

    public function turnRight();
}