<?php


namespace ToyRobot;


interface BoardInterface
{
    public function processAndHandleAction(string $actionLine);
}