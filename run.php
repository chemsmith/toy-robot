<?php

use ToyRobot\Simulation;

require_once __DIR__ . '/vendor/autoload.php';

$simulation = new Simulation();
$simulation->run($argv);
