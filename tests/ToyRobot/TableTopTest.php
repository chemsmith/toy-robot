<?php

namespace ToyRobot;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;


class TableTopTest extends TestCase
{
    private TableTop $tableTop;
    private MockObject $toyMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->toyMock = $this->createMock(Robot::class);
        $this->tableTop = new TableTop($this->toyMock);
    }

    public function testProcessAndHandleActionWithValidPlace()
    {
        $this->toyMock->expects($this->once())
            ->method('setLocation')
            ->with(['x'=>1,'y'=>1])
            ->willReturn(true);
        $this->toyMock->expects($this->once())
            ->method('setOrientation')
            ->with('NORTH');
        $this->tableTop->processAndHandleAction('PLACE 1,1,NORTH');
    }

    public function testProcessAndHandleActionWithInvalidPlace()
    {
        $this->toyMock->expects($this->once())
            ->method('setLocation')
            ->with(['x'=>5,'y'=>5])
            ->willReturn(false);
        $this->toyMock->expects($this->never())
            ->method('setOrientation')
            ->with('NORTH');
        $this->tableTop->processAndHandleAction('PLACE 5,5,NORTH');
    }

    public function testProcessAndHandleActionWithMove()
    {
        $this->toyMock->method('getLocation')
            ->willReturn(['x'=>1, 'y'=>1]);

        $this->toyMock->expects($this->once())
            ->method('move');
        $this->tableTop->processAndHandleAction('MOVE');
    }

    public function testProcessAndHandleActionWithLeft()
    {
        $this->toyMock->method('getLocation')
            ->willReturn(['x'=>1, 'y'=>1]);

        $this->toyMock->expects($this->once())
            ->method('turnLeft');
        $this->tableTop->processAndHandleAction('LEFT');
    }

    public function testProcessAndHandleActionWithRight()
    {
        $this->toyMock->method('getLocation')
            ->willReturn(['x'=>1, 'y'=>1]);

        $this->toyMock->expects($this->once())
            ->method('turnRight');
        $this->tableTop->processAndHandleAction('RIGHT');
    }

    public function testProcessAndHandleActionWithReport()
    {
        $this->toyMock->method('getLocation')
            ->willReturn(['x'=>1, 'y'=>2]);

        $this->toyMock->method('getOrientation')
            ->willReturn('SOUTH');

        $this->expectOutputString("1,2,SOUTH\n");
        $this->tableTop->processAndHandleAction('REPORT');
    }
}
