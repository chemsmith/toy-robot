<?php

namespace ToyRobot;

use PHPUnit\Framework\TestCase;

final class RobotTest extends TestCase
{
    private Robot $robot;

    private static array $movementDirectionMatrix = [
        'NORTH' => ['x' => 0, 'y' => 1],
        'EAST' => ['x' => 1, 'y' => 0],
        'SOUTH' => ['x' => 0, 'y' => -1],
        'WEST' => ['x' => -1, 'y' => 0],
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->robot = new Robot();
        $this->robot->setPlayingBoardSize(5);
        $this->robot->setPlayingBoardMovements(self::$movementDirectionMatrix);
        $startingLocation = ['x' => 0, 'y' => 0];
        $startingOrientation = 'NORTH';
        // corresponds to command "PLACE 0,0,NORTH"
        $this->robot->setLocation($startingLocation);
        $this->robot->setOrientation($startingOrientation);
    }

    public function testGetOrientation()
    {
        $orientation = $this->robot->getOrientation();
        $this->assertEquals('NORTH', $orientation, 'Robot orientation incorrect, should be starting orientation of "NORTH".');
    }

    public function testGetLocation()
    {
        $location = $this->robot->getLocation();
        $this->assertEquals(['x' => 0, 'y' => 0], $location, 'Robot location incorrect, should be starting location of 0,0.');
    }

    public function testTurnRight()
    {
        $this->robot->turnRight(); // EAST
        $orientation = $this->robot->getOrientation();
        $this->assertEquals('EAST', $orientation, 'Robot orientation incorrect, should be "EAST" after one right rotation.');
        $this->robot->turnRight(); // SOUTH
        $this->robot->turnRight(); // WEST
        $this->robot->turnRight(); // NORTH
        $orientation = $this->robot->getOrientation();
        $this->assertEquals('NORTH', $orientation, 'Robot orientation incorrect, should be back at "NORTH" after 4 right rotations.');

    }

    public function testTurnLeft()
    {
        $this->robot->turnLeft(); // WEST
        $orientation = $this->robot->getOrientation();
        $this->assertEquals('WEST', $orientation, 'Robot orientation incorrect, should be "WEST" after one left rotation.');
        $this->robot->turnLeft(); // SOUTH
        $this->robot->turnLeft(); // EAST
        $this->robot->turnLeft(); // NORTH
        $orientation = $this->robot->getOrientation();
        $this->assertEquals('NORTH', $orientation, 'Robot orientation incorrect, should be back at "NORTH" after 4 left rotations.');
    }

    public function testMove()
    {
        $this->robot->move();
        $currentLocation = $this->robot->getLocation();
        $this->assertEquals(['x' => 0, 'y' => 1], $currentLocation, 'Robot didn\'t move NORTH one position');
        $this->robot->move(); // y = 2
        $this->robot->move(); // y = 3
        $this->robot->move(); // y = 4
        $this->robot->move(); // y = 5? if moved past edge of surface
        $currentLocation = $this->robot->getLocation();
        $this->assertEquals(['x' => 0, 'y' => 4], $currentLocation, 'Robot moved off the edge of the playing surface.');
        $this->robot->turnLeft(); // x = 0
        $this->robot->move(); // x = -1? if moved past edge of surface
        $this->robot->move(); // x = -2? if moved past edge of surface
        $currentLocation = $this->robot->getLocation();
        $this->assertEquals(['x' => 0, 'y' => 4], $currentLocation, 'Robot moved off the edge of the playing surface.');
        $this->robot->turnRight(); // x = 0
        $this->robot->turnRight(); // x = 0
        $this->robot->move(); // x = 1
        $this->robot->move(); // x = 2
        $currentLocation = $this->robot->getLocation();
        $this->assertEquals(['x' => 2, 'y' => 4], $currentLocation, 'Robot didn\'t move in the x-axis.');
        $this->robot->turnRight();
        $this->robot->move(); // y = 3
        $this->robot->move(); // y = 2
        $currentLocation = $this->robot->getLocation();
        $this->assertEquals(['x' => 2, 'y' => 2], $currentLocation, 'Robot didn\'t correctly reverse direction.');
    }
}
