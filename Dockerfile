FROM php:7.4-cli-alpine

COPY . /app/
WORKDIR /app

ENTRYPOINT [ "php", "run.php" ]