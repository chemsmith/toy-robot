# Toy Robot Simulator

## Table of contents:

* [Problem description](./README.md#problem-description)
* [Setup](./README.md#setup)
* [Simulator](./README.md#simulator)
* [Tests](./README.md#tests)
* [Examples](./README.md#examples)

## Problem description
Create an application that can read in commands of the following form:

    PLACE X,Y,F
    MOVE
    LEFT
    RIGHT
    REPORT

- The application is a simulation of a toy robot moving on a square tabletop,
  of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be
  prevented from falling to destruction. Any movement that would result in the
  robot falling from the table must be prevented, however further valid
  movement commands must still be allowed.
- PLACE will put the toy robot on the table in position X,Y and facing NORTH,
  SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any
  sequence of commands may be issued, in any order, including another PLACE
  command. The application should discard all commands in the sequence until
  a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is
  currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction
  without changing the position of the robot.
- REPORT will announce the X,Y and F of the robot. This can be in any form,
  but standard output is sufficient.
- A robot that is not on the table can choose the ignore the MOVE, LEFT, RIGHT
  and REPORT commands.
- Input can be from a file, or from standard input, as the developer chooses.
- Provide test data to exercise the application.

### Constraints
- The toy robot must not fall off the table during movement. This also
  includes the initial placement of the toy robot.
- Any move that would cause the robot to fall must be ignored.

## Setup

1. Clone this repository:

       git clone https://gitlab.com/chemsmith/toy-robot.git

2. Change directory into the repository directory:

       cd toy-robot

3. Install [composer](https://getcomposer.org/) managed requirements (this is required both for testing and autoloading our toy robot classes)

       composer install

## Simulator
The simulator can be given commands either via the CLI/STDIN or from a file:

### Locally
    # STDIN
    php run.php

    # FILE
    php run.php examples/example[A-D]
### Docker
This repository also contains a Dockerfile to build the app as a container. Using Gitlab CI this container is tested and built every time the repository is updated.
#### Building/obtaining
To simply use the image built by Gitlab CI.
    
    docker pull registry.gitlab.com/chemsmith/toy-robot
    docker tag registry.gitlab.com/chemsmith/toy-robot toy-tobot
Alternatively docker image can be built locally

    docker build -t toy-robot .
#### Using
    # STDIN:
    docker run -it --rm toy-robot
    
    # FILE:
    docker run --rm toy-robot examples/example[A-D]
## Tests
Test are run using PHPUnit 9.2 (installed with our initial `composer install`).

    ./vendor/bin/phpunit tests
## Examples
The repository includes four example files with sample commands.

    php run.php examples/exampleA
    // Expected output: 0,1,NORTH

    php run.php examples/exampleB
    // Expected output: 0,0,WEST

    php run.php examples/exampleC
    // Expected output: 3,3,NORTH

    php run.php examples/exampleD
    // Expected output: 4,3,NORTH
